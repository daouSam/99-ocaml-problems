module Problem01 = struct
  let rec last (xs: 'a list): 'a option =
    match xs with
    | [] -> None
    | [x] -> Some x
    | _ :: rest -> last rest

  let () =
    print_endline "Testing Problem 01";
    assert (last [1; 2; 3; 4] = Some 4);
    assert (last [] = None)
end

module Problem02 = struct
  let rec last_two (xs: 'a list): ('a * 'a) option =
    match xs with
    | [] -> None
    | [_] -> None
    | [x; y] -> Some (x, y)
    | _ :: rest -> last_two rest

  let () =
    print_endline "Testing Problem 02";
    assert (last_two [1; 2; 3; 4] = Some (3, 4));
    assert (last_two [1] = None);
    assert (last_two [] = None)
end

module Problem03 = struct
  let rec at (k: int) (xs: 'a list): 'a option =
    match xs with
    | x :: _    when k == 1 -> Some x
    | _ :: rest when k > 1 -> at (k - 1) rest
    | _ -> None

  let () =
    print_endline "Testing Problem 03";
    assert (at 3 [1; 2; 3; 4] = Some 3);
    assert (at 0 [1; 2; 3; 4] = None);
    assert (at 5 [1; 2; 3; 4] = None)
end

module Problem04 = struct
  let rec length (xs: 'a list): int =
    match xs with
    | [] -> 0
    | _ :: rest -> 1 + length rest

  let length_tail (xs: 'a list): int =
    let rec length_tail' (xs: 'a list) (result: int): int =
      match xs with
      | [] -> result
      | _ :: rest -> length_tail' rest (result + 1)
    in length_tail' xs 0

  let best_length (xs: 'a list): int =
    let result = ref 0 in
    let ys = ref xs in
    while !ys != [] do
      result := !result + 1;
      ys := List.tl !ys
    done;
    !result

  let () =
    print_endline "Testing Problem 04";
    let test (length: 'a list -> int) =
      assert (length []           = 0);
      assert (length [1]          = 1);
      assert (length [1; 2]       = 2);
      assert (length [1; 2; 3]    = 3);
      assert (length [1; 2; 3; 4] = 4)
    in
    test length;
    test length_tail;
    test best_length
end

module Problem05 = struct
  let rev (xs: 'a list): 'a list =
    let rec rev' (xs: 'a list) (acc: 'a list): 'a list =
      match xs with
      | [] -> acc
      | x :: rest -> rev' rest (x :: acc)
    in rev' xs []

  let () =
    print_endline "Testing Problem 05";
    assert (rev [1; 2; 3; 4] = [4; 3; 2; 1])
end

module Problem06 = struct
  let is_palindrom (xs: 'a list): bool = xs = (List.rev xs)

  let () =
    print_endline "Testing Problem 06";
    assert (not (is_palindrom [1; 2; 3; 4]));
    assert (is_palindrom [1; 2; 2; 1])
end

module Problem07 = struct
  type 'a node =
    | One of 'a
    | Many of 'a node list

  let flatten (xs: 'a node list): 'a list =
    let rec flatten_node (x: 'a node): 'a list =
      match x with
      | One y -> [y]
      | Many ys -> ys |> List.map flatten_node |> List.concat
    in
    flatten_node (Many xs)

  let () =
    print_endline "Testing Problem 07";
    assert (flatten [One "a"; Many [One "b"; Many [One "c" ;One "d"]; One "e"]] = ["a"; "b"; "c"; "d"; "e"])
end

module Problem08 = struct
  let compress (xs: 'a list): 'a list =
    let rec compress' (x: 'a) (xs: 'a list) =
      match xs with
      | [] -> []
      | y :: ys ->
         if x = y
         then compress' y ys
         else y :: compress' y ys
    in
    match xs with
    | [] -> []
    | x :: xs -> x :: compress' x xs

  let () =
    print_endline "Testing Problem 08";
    assert (compress [1; 2; 3; 4] = [1; 2; 3; 4]);
    assert (compress [1; 2; 2; 3; 3; 3; 4; 4; 4; 4] = [1; 2; 3; 4]);
    assert (compress [] = [])
end

module Problem09 = struct
  let pack (xs: 'a list): 'a list list =
    let rec pack' (p: 'a list) (xs: 'a list): 'a list list =
      (* TODO: try to get rid of this assert *)
      assert (p != []);
      match xs with
      | [] -> [p]
      | y :: xs ->
         if List.hd p = y
         then pack' (y :: p) xs
         else p :: pack' [y] xs
    in
    match xs with
    | [] -> []
    | x :: xs -> pack' [x] xs

  let () =
    print_endline "Testing Problem 09";
    let input = ["a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "d"; "e"; "e"; "e"; "e"] in
    let output = [["a"; "a"; "a"; "a"]; ["b"]; ["c"; "c"]; ["a"; "a"]; ["d"; "d"]; ["e"; "e"; "e"; "e"]] in
    assert (pack input = output);
    assert (pack [] = [])
end

module Problem10 = struct
  let encode (xs: 'a list): (int * 'a) list =
    let rec encode' (count: int) (x: 'a) (xs: 'a list): (int * 'a) list =
      match xs with
      | [] -> [(count, x)]
      | y :: ys ->
         if x = y
         then encode' (count + 1) y ys
         else (count, x) :: encode' 1 y ys
    in
    match xs with
    | [] -> []
    | x :: xs -> encode' 1 x xs

  let () =
    print_endline "Testing Problem 10";
    let input = ["a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e"] in
    let output = [(4, "a"); (1, "b"); (2, "c"); (2, "a"); (1, "d"); (4, "e")] in
    assert (encode input = output);
    assert (encode [] = [])
end

module Problem11 = struct
  type 'a rle =
    | One of 'a
    | Many of int * 'a

  let encode (xs: 'a list): 'a rle list =
    let rec encode' (count: int) (x: 'a) (xs: 'a list): 'a rle list =
      let result () =
        assert (count > 0);
        if count == 1 then One x else Many (count, x)
      in
      match xs with
      | [] -> [result ()]
      | y :: ys ->
         if x = y
         then encode' (count + 1) y ys
         else result () :: encode' 1 y ys
    in
    match xs with
    | [] -> []
    | x :: xs -> encode' 1 x xs

  let () =
    print_endline "Testing Problem 11";
    let input = ["a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e"] in
    let output = [Many (4, "a"); One "b"; Many (2, "c"); Many (2, "a"); One "d"; Many (4, "e")] in
    assert (encode input = output);
    assert (encode [] = [])
end

module Problem12 = struct
  type 'a rle =
    | One of 'a
    | Many of int * 'a

  let rec decode (xs: 'a rle list): 'a list =
    match xs with
    | [] -> []
    | One x :: xs -> x :: decode xs
    | Many (count, x) :: xs when count <= 0 -> decode xs
    | Many (count, x) :: xs -> x :: decode (Many (count - 1, x) :: xs)

  let () =
    print_endline "Testing Problem 12";
    let input = [Many (4, "a"); One "b"; Many (2, "c"); Many (2, "a"); One "d"; Many (4, "e")] in
    let output = ["a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e"] in
    assert (decode input = output);
    assert (decode [] = [])
end

module Problem13 = struct
  (* I solved it like that initially *)

  open Problem11

  let () =
    print_endline "Testing Problem 13";
    let input = ["a"; "a"; "a"; "a"; "b"; "c"; "c"; "a"; "a"; "d"; "e"; "e"; "e"; "e"] in
    let output = [Many (4, "a"); One "b"; Many (2, "c"); Many (2, "a"); One "d"; Many (4, "e")] in
    assert (encode input = output);
    assert (encode [] = [])
end

module Problem14 = struct
  let rec duplicate (xs: 'a list): 'a list =
    match xs with
    | [] -> []
    | x :: xs -> x :: x :: duplicate xs

  let () =
    print_endline "Testing Problem 14";
    assert (duplicate [1; 2; 3; 4] = [1; 1; 2; 2; 3; 3; 4; 4]);
    assert (duplicate [] = [])
end

module Problem15 = struct
  let rec generate (count: int) (x: 'a): 'a list =
    if count <= 0
    then []
    else x :: generate (count - 1) x

  let rec replicate (xs: 'a list) (count: int): 'a list =
    match xs with
    | [] -> []
    | x :: xs -> List.concat [generate count x; replicate xs count]

  let () =
    print_endline "Testing Problem 15";
    assert (replicate [1; 2; 3; 4] 0 = []);
    assert (replicate [1; 2; 3; 4] 1 = [1; 2; 3; 4]);
    assert (replicate [1; 2; 3; 4] 2 = [1; 1; 2; 2; 3; 3; 4; 4]);
    assert (replicate [1; 2; 3; 4] 3 = [1; 1; 1; 2; 2; 2; 3; 3; 3; 4; 4; 4])
end
